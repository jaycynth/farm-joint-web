import React, { useState, useContext, useEffect } from "react";
import { GlobalContext } from "../../../context/Provider";
import { useHistory } from "react-router-dom";
import clearCreateHealth from "../../../context/actions/health/clearCreateHealth";
import createHealth from "../../../context/actions/health/createHealth";
import CreateHealthRecordUI from "../../../layouts/Healthrecord/Create";

const CreateHealthRecordContainer = () => {
  const [form, setForm] = useState({});
  const history = useHistory();

  const {
    healthDispatch,
    healthState: {
      addHealth: { loading, error, data },
    },
  } = useContext(GlobalContext);

  const formIsHalfFilled =
    Object.values(form).filter((item) => item !== "").length > 0 && !data;

  const farmanimalId = JSON.parse(localStorage.getItem("FARMANIMALID"));

  useEffect(() => {
    if (data) {
      history.push("/healthrecord?farmanimal_id=" + farmanimalId);
    }
    return () => {
      clearCreateHealth()(healthDispatch);
    };
  }, [data, farmanimalId, healthDispatch, history]);

  const onChange = (e, { name, value }) => {
    setForm({ ...form, [name]: value });
  };

  console.log("form", form);

  const onSubmit = () => {
    createHealth(form)(healthDispatch);
  };

  const formInValid =
    !form.date?.length ||
    !form.proceduretype?.length ||
    !form.description?.length ||
    !form.vetname?.length;

  return (
    <CreateHealthRecordUI
      onSubmit={onSubmit}
      formInvalid={formInValid}
      onChange={onChange}
      form={form}
      formIsHalfFilled={formIsHalfFilled}
      loading={loading}
    />
  );
};

export default CreateHealthRecordContainer;
