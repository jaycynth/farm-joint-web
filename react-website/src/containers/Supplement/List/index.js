import React, { useContext, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { GlobalContext } from "../../../context/Provider";
import getSupplement from "../../../context/actions/supplement/getSupplement";
import SupplementListUI from "../../../layouts/Supplement/List";

const SupplementContainer = () => {
  const { supplementDispatch, supplementState } = useContext(GlobalContext);
  const history = useHistory();

  console.log("supplementState", supplementState);

  useEffect(() => {
    getSupplement(history)(supplementDispatch);
  }, [history, supplementDispatch]);
  return <SupplementListUI state={supplementState} />;
};

export default SupplementContainer;
