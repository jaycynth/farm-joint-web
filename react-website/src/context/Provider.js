//application state
import React, { createContext, useReducer } from "react";
import auth from "./reducers/auth";
import produce from "./reducers/produce";
import farmanimal from "./reducers/farmanimal";
import feed from "./reducers/feed";
import supplement from "./reducers/supplement";
import purchase from "./reducers/purchase";
import health from "./reducers/health"
import authInitialState from "./initialstates/authInitialState";
import produceInitialState from "./initialstates/produceInitialState";
import farmanimalInitialState from "./initialstates/farmanimalInitialState";
import feedInitialState from "./initialstates/feedInitialState";
import supplementInitialState from "./initialstates/supplementInitialState";
import purchaseInitialState from "./initialstates/purchaseInitialState";
import healthInitialState from "./initialstates/healthInitialState"

export const GlobalContext = createContext({});

export const GlobalProvider = ({ children }) => {
  const [authState, authDispatch] = useReducer(auth, authInitialState);
  const [produceState, produceDispatch] = useReducer(
    produce,
    produceInitialState
  );
  const [farmanimalState, farmanimalDispatch] = useReducer(
    farmanimal,
    farmanimalInitialState
  );
  const [feedState, feedDispatch] = useReducer(feed, feedInitialState);
  const [supplementState, supplementDispatch] = useReducer(
    supplement,
    supplementInitialState
  );

  const [purchaseState, purchaseDispatch] = useReducer(
    purchase,
    purchaseInitialState
  );

  const [healthState, healthDispatch] = useReducer(
    health,
    healthInitialState
  );


  return (
    <GlobalContext.Provider
      value={{
        authState,
        authDispatch,
        produceState,
        produceDispatch,
        farmanimalState,
        farmanimalDispatch,
        feedState,
        feedDispatch,
        supplementState,
        supplementDispatch,
        purchaseState,
        purchaseDispatch,
        healthState,
        healthDispatch
      }}
    >
      {children}
    </GlobalContext.Provider>
  );
};
