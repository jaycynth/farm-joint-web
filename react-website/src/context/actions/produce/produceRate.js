/* eslint-disable import/no-anonymous-default-export */
import {
    PRODUCE_RATE_LOADING,
    PRODUCE_RATE_LOAD_ERROR,
    PRODUCE_RATE_LOAD_SUCCESS,
} from "../../../constants/actionTypes";
import { CONNECTION_ERROR } from "../../../constants/api";
import axiosInstance from "../../../helpers/axios";

export default () => (dispatch) => {
      var year = new Date().getFullYear()
      var month = new Date().getMonth()

      var arr = [];

     var timestamp1 = new Date().getTime()/1000
     console.log("arr", Math.trunc(timestamp1))

     arr.push(Math.trunc(timestamp1))

     for (let i = 0; i < 3; i++){
        var timestamp = new Date(year,month,0).getTime()/1000
        arr.push(timestamp)


         if(month === 0){
             year = year-1
             month = 11
         }else{
             month = month-1
         }

     }
    
     console.log("arr", arr)


    dispatch({
        type: PRODUCE_RATE_LOADING,
    });
    axiosInstance()
        .post("/produce/produce_rate", {
            rate_filters: [{
                    rate_id: "JAN",
                    start_timestamp: arr[3],
                    end_timestamp: arr[2],
                },
                {
                    rate_id: "FEB",
                    start_timestamp: arr[2],
                    end_timestamp: arr[1],
                },
                {
                    rate_id: "MAR",
                    start_timestamp: arr[1],
                    end_timestamp: arr[0],
                },
            ],
        })
        .then((res) => {
            dispatch({
                type: PRODUCE_RATE_LOAD_SUCCESS,
                payload: res.data,
            });
            console.log("res", res.data);
            localStorage.setItem("DATALOAD", JSON.stringify(res.data))
        })
        .catch((err) => {
            dispatch({
                type: PRODUCE_RATE_LOAD_ERROR,
                payload: err.response ? err.response.data : CONNECTION_ERROR,
            });
        });
};