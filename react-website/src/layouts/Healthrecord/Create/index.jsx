import React from "react";

import { Grid, Card, Form, Header as SemanticHeader, Button } from "semantic-ui-react";
import Header from "../../../components/Header";
import { Prompt } from "react-router-dom";


const CreateHealthRecordUI = ({
  loading,
  onChange,
  formIsHalfFilled,
  formInvalid,
  onSubmit,
}) => {
  return (
    <div>
      <Prompt
        when={formIsHalfFilled}
        message={JSON.stringify({
          header: "confirm",
          content: "You have unsavd changes; You sure you want to leave?",
        })}
      />
      <Header />
      <Grid centered>
        <Grid.Column className="form-column">
          <SemanticHeader>Add a new health record</SemanticHeader>
          <Card fluid>
            <Card.Content>
              <Form unstackable>
                <Form.Input
                  label="Date"
                  placeholder="Date"
                  name="date"
                  onChange={onChange}
                />
                <Form.Input
                  label="Procedure Type"
                  placeholder="Procedure Type"
                  name="proceduretype"
                  onChange={onChange}
                />
                <Form.TextArea
                  label="Description"
                  placeholder="Description"
                  name="description"
                  input= "text"
                  onChange={onChange}
                />

                <Form.Input
                  label="Vet Name"
                  placeholder="Vet Name"
                  name="vetname"
                  onChange={onChange}
                />

                <Button
                  primary
                  fluid
                  disabled={formInvalid || loading}
                  onClick={onSubmit}
                  loading={loading}
                  type="submit"
                >
                  Submit
                </Button>
              </Form>
            </Card.Content>
          </Card>
        </Grid.Column>
      </Grid>
    </div>
  );
};

export default CreateHealthRecordUI;
