import {
  LOGIN_ERROR,
  LOGIN_LOADING,
  LOGIN_SUCCESS,
} from "../../../constants/actionTypes";
import { CONNECTION_ERROR } from "../../../constants/api";
import axiosInstance from "../../../helpers/axios";

export const login = ({ username: user_name, phonenumber: phone_number }) => (
  dispatch
) => {
  dispatch({
    type: LOGIN_LOADING,
  });

  axiosInstance()
    .post("/users/action/login", {
      user: {
        user_name: user_name,
        phone_number: phone_number,
      },
    })
    .then((res) => {
      localStorage.token = res.data.jwt_token;
      dispatch({
        type: LOGIN_SUCCESS,
        payload: res.data,
      });
    })
    .catch((err) => {
      dispatch({
        type: LOGIN_ERROR,
        payload: err.response ? err.response.data : CONNECTION_ERROR,
      });
    });
};
