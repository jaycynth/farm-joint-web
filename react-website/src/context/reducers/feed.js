import {
  FEED_LOADING,
  FEED_LOAD_SUCCESS,
  FEED_LOAD_ERROR,
  ADD_FEED_LOADING,
  ADD_FEED_LOAD_ERROR,
  ADD_FEED_LOAD_SUCCESS,
  CLEAR_ADD_FEED,
  SEARCH_FEED,
} from "../../constants/actionTypes";
const feed = (state, { payload, type }) => {
  switch (type) {
    case SEARCH_FEED: {
      const searchValue = payload?.toLowerCase();
      return {
        ...state,
        feed: {
          ...state.feed,
          loading: false,
          isSearchActive: !!payload.length > 0 || false,
          foundContacts: state.feed.data?.filter((item) => {
            try {
              console.log(
                "TAGG",
                item.name.toLowerCase().search(searchValue) !== -1
              );
              return item.name.toLowerCase().search(searchValue) !== -1;
            } catch (error) {
              console.log("TAGG", error);
              return [];
            }
          }),
        },
      };
    }
    case FEED_LOADING: {
      return {
        ...state,
        feed: {
          ...state.feed,
          loading: true,
        },
      };
    }

    case FEED_LOAD_SUCCESS: {
      return {
        ...state,
        feed: {
          ...state.feed,
          loading: false,
          data: payload,
        },
      };
    }

    case FEED_LOAD_ERROR: {
      return {
        ...state,
        feed: {
          ...state.feed,
          loading: false,
          error: payload,
        },
      };
    }
    case ADD_FEED_LOADING: {
      return {
        ...state,
        addFeed: {
          ...state.addFeed,
          error: null,
          loading: true,
        },
      };
    }
    case ADD_FEED_LOAD_SUCCESS: {
      return {
        ...state,
        addFeed: {
          ...state.addFeed,
          loading: false,
          data: payload,
        },
        feed: {
          ...state.feed,
          loading: false,
          data: [payload, ...state.feed.data],
        },
      };
    }
    case ADD_FEED_LOAD_ERROR: {
      return {
        ...state,
        addFeed: {
          ...state.addFeed,
          loading: false,
        },
      };
    }

    case CLEAR_ADD_FEED: {
      return {
        ...state,
        addFeed: {
          ...state.addFeed,
          error: null,
          loading: false,
          data: null,
        },
      };
    }
    default:
      return state;
  }
};

export default feed;
