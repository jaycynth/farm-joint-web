import React, { useContext } from "react";
import Header from "../../../components/Header";
import { Link } from "react-router-dom";
import searchFarmanimal from "../../../context/actions/farmanimal/searchFarmanimal";
import { GlobalContext } from "../../../context/Provider";

import {
  Placeholder,
  Container,
  Table,
  Menu,
  Icon,
  Button,
  Input,
} from "semantic-ui-react";
import "./index.css";

const FarmanimalListUI = ({
  state: {
    farmanimal: { loading, error, data, isSearchActive, foundContacts },
  },
}) => {
  const { farmanimalDispatch: dispatch } = useContext(GlobalContext);

  const onChange = (e, { value }) => {
    const searchText = value.trim().replace(/" "/g, "");
    searchFarmanimal(searchText)(dispatch);
  };

  const BarStyling = {
    width: "20rem",
    background: "#F2F1F9",
    border: "none",
    padding: "0.5rem",
  };

  return (
    <div>
      <Header />
      <Container className="farmanimal-container">
        {loading && (
          <>
            {""}
            <Placeholder>
              <Placeholder.Header image>
                <Placeholder.Line />
                <Placeholder.Line />
              </Placeholder.Header>
              <Placeholder.Paragraph>
                <Placeholder.Line />
                <Placeholder.Line />
                <Placeholder.Line />
                <Placeholder.Line />
              </Placeholder.Paragraph>
            </Placeholder>
          </>
        )}
        <Menu secondary>
          <Menu.Item>
            <Button as={Link} to="/add/farmanimal" primary basic icon>
              <Icon name="add"></Icon>
              Add Farm animal
            </Button>
          </Menu.Item>
          <Menu.Menu position="right">
            <Menu.Item>
              <Input
                icon="search"
                style={BarStyling}
                onChange={onChange}
                placeholder="Search..."
              />
            </Menu.Item>
          </Menu.Menu>
        </Menu>
        <h4>Farm Animal Records</h4>
        <Table singleLine>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell>Birth Date</Table.HeaderCell>
              <Table.HeaderCell>Tag</Table.HeaderCell>
              <Table.HeaderCell>Status</Table.HeaderCell>
              <Table.HeaderCell>Calfing Count</Table.HeaderCell>
              <Table.HeaderCell>View Health Record</Table.HeaderCell>
            </Table.Row>
          </Table.Header>

          <Table.Body>
            {isSearchActive
              ? foundContacts?.length > 0 &&
                foundContacts.map((farmanimal) => (
                  <Table.Row key={farmanimal.farmanimal_id}>
                    <Table.Cell>{farmanimal.birth_date}</Table.Cell>
                    <Table.Cell>{farmanimal.tag}</Table.Cell>
                    <Table.Cell>{farmanimal.status}</Table.Cell>
                    <Table.Cell>{farmanimal.calfingCount}</Table.Cell>
                    <Table.Cell>
                      <Link to ={"/healthrecord?farmanimal_id" + farmanimal.farmanimal_id}>
                        <Icon name="file" color="green" link="true"></Icon>
                      </Link>
                    </Table.Cell>
                  </Table.Row>
                ))
              : data?.length > 0 &&
                data.map((farmanimal) => (
        
                  <Table.Row key={farmanimal.farmanimal_id}>
                    <Table.Cell>{farmanimal.birth_date}</Table.Cell>
                    <Table.Cell>{farmanimal.tag}</Table.Cell>
                    <Table.Cell>{farmanimal.status}</Table.Cell>
                    <Table.Cell>{farmanimal.calfingCount}</Table.Cell>
                    <Table.Cell> 
                    <Link to ={"/healthrecord?farmanimal_id=" + farmanimal.farmanimal_id}>
                        <Icon name="file" color="green" link="true"></Icon>
                      </Link>
                    </Table.Cell>
                  </Table.Row>
                ))}
          </Table.Body>
        </Table>
      </Container>
    </div>
  );
};

export default FarmanimalListUI;
