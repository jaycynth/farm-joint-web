/* eslint-disable import/no-anonymous-default-export */
import {
    ADD_HEALTH_LOADING,
    ADD_HEALTH_LOAD_ERROR,
    ADD_HEALTH_LOAD_SUCCESS,
  } from "../../../constants/actionTypes";
  import { CONNECTION_ERROR } from "../../../constants/api";
  import axiosInstance from "../../../helpers/axios";
  
  export default ({
    date,
    proceduretype: procedure_type,
    description,
    vetname: vet_name,
  }) => (dispatch) => {

        const fi = JSON.parse(localStorage.getItem("FARMANIMALID"))

    dispatch({
      type: ADD_HEALTH_LOADING,
    });
  
    axiosInstance()
      .post("/health", {
        health: {
          date: date,
          procedure_type: procedure_type,
          description: description,
          vet_name: vet_name,
          farmanimal_id : fi
        },
      })
      .then((res) => {
        console.log("RESPONSE", res);
  
        dispatch({
          type: ADD_HEALTH_LOAD_SUCCESS,
          payload: res.data,
        });
      })
      .catch((err) => {
        console.log("ERROR", err);
  
        dispatch({
          type: ADD_HEALTH_LOAD_ERROR,
          payload: err.response ? err.response.data : CONNECTION_ERROR,
        });
      });
  };
  