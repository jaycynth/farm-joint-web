/* eslint-disable import/no-anonymous-default-export */
import {
  FARMANIMAL_LOADING,
  FARMANIMAL_LOAD_ERROR,
  FARMANIMAL_LOAD_SUCCESS,
} from "../../../constants/actionTypes";
import { CONNECTION_ERROR } from "../../../constants/api";
import axiosInstance from "../../../helpers/axios";

export default (history) => (dispatch) => {
  dispatch({
    type: FARMANIMAL_LOADING,
  });
  axiosInstance(history)
    .get("/farmanimal")
    .then((res) => {
      dispatch({
        type: FARMANIMAL_LOAD_SUCCESS,
        payload: res.data.farmanimal_list,
      });
    })
    .catch((err) => {
      dispatch({
        type: FARMANIMAL_LOAD_ERROR,
        payload: err.response ? err.response.data : CONNECTION_ERROR,
      });
    });
};
