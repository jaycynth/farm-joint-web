import React, { useContext, useEffect, useState } from "react";
import createProduce from "../../../context/actions/produce/createProduce";
import { GlobalContext } from "../../../context/Provider";
import { useHistory } from "react-router-dom";
import clearCreateProduce from "../../../context/actions/produce/clearCreateProduce";
import CreateProduceUI from "../../../layouts/Produce/CreateProduce";
const CreateProduceContainer = () => {
  const [form, setForm] = useState({});
  const history = useHistory();

  const {
    produceDispatch,
    produceState: {
      addProduce: { loading, error, data },
    },
  } = useContext(GlobalContext);

  const formIsHalfFilled =
    Object.values(form).filter((item) => item !== "").length > 0 && !data;

  useEffect(() => {
    if (data) {
      history.push("/produce");
    }
    return () => {
      clearCreateProduce()(produceDispatch);
    };
  }, [data, history, produceDispatch]);

  const onChange = (e, { name, value }) => {
    setForm({ ...form, [name]: value });
  };

  console.log("form", form);

  const onSubmit = () => {
    createProduce(form)(produceDispatch);
  };

  const formInValid =
    !form.producetype?.length ||
    !form.date?.length ||
    !form.quantity?.length ||
    !form.quantitytype?.length ||
    !form.eachprice?.length ||
    !form.farmanimal?.length;

  return (
    <CreateProduceUI
      onSubmit={onSubmit}
      formInvalid={formInValid}
      onChange={onChange}
      form={form}
      formIsHalfFilled={formIsHalfFilled}
      loading={loading}
    />
  );
};

export default CreateProduceContainer;
