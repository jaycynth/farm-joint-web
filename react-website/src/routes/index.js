import { lazy } from "react";
import RegisterComponent from "../containers/Register";
import LoginComponent from "../containers/Login";
import ProduceComponent from "../containers/Produce/List";
import CreateProduceContainer from "../containers/Produce/Create";
import CreateFarmanimalContainer from "../containers/Farmanimal/Create";
import FarmanimalContainer from "../containers/Farmanimal/List";
import FeedContainer from "../containers/Feed/List";
import SupplementContainer from "../containers/Supplement/List";
import HomeContainer from "../containers/Home";
import HealthrecordContainer from "../containers/Healthrecord/List";
import CreateFeedContainer from "../containers/Feed/Create";
import CreateSupplementContainer from "../containers/Supplement/Create";
import StatisticsContainer from "../containers/Statistics";
import CreateHealthRecordContainer from "../containers/Healthrecord/Create";
const routes = [
  {
    path: "/auth/register",
    component: RegisterComponent,
    title: "Register",
    needsAuth: false,
  },

  {
    path: "/auth/login",
    component: LoginComponent,
    title: "Login",
    needsAuth: false,
  },
  {
    path: "/add/produce",
    component: CreateProduceContainer,
    title: "Create Produce",
    needsAuth: true,
  },
  {
    path: "/add/farmanimal",
    component: CreateFarmanimalContainer,
    title: "Create Farm Animal",
    needsAuth: true,
  },
  {
    path: "/add/feed",
    component: CreateFeedContainer,
    title: "Create Feed Record",
    needsAuth: true,
  },
  {
    path: "/add/supplement",
    component: CreateSupplementContainer,
    title: "Create Supplement Record",
    needsAuth: true,
  },
  {
    path: "/add/health",
    component: CreateHealthRecordContainer,
    title: "Create Health Record",
    needsAuth: true,
  },
  {
    path: "/farmanimal",
    component: FarmanimalContainer,
    title: "Farm Animal",
    needsAuth: true,
  },
  {
    path: "/feed",
    component: FeedContainer,
    title: "Feed",
    needsAuth: true,
  },
  {
    path: "/supplement",
    component: SupplementContainer,
    title: "Supplement",
    needsAuth: true,
  },

  {
    path: "/produce",
    component: ProduceComponent,
    title: "Produce",
    needsAuth: true,
  },
  {
    path: "/healthrecord",
    component: HealthrecordContainer,
    title: "Health record",
    needsAuth: true,
  },
  {
    path: "/statistics",
    component: StatisticsContainer,
    title: "Statistics",
    needsAuth: true,
  },
  {
    path: "/",
    component: HomeContainer,
    title: "Home",
    needsAuth: false,
  },
];

export default routes;
