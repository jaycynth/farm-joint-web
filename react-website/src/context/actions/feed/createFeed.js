/* eslint-disable import/no-anonymous-default-export */
import {
  ADD_FEED_LOADING,
  ADD_FEED_LOAD_ERROR,
  ADD_FEED_LOAD_SUCCESS,
} from "../../../constants/actionTypes";
import { CONNECTION_ERROR } from "../../../constants/api";
import axiosInstance from "../../../helpers/axios";

export default ({
  date: date,
  name: name,
  quantity: quantity,
  price: price,
}) => (dispatch) => {
  dispatch({
    type: ADD_FEED_LOADING,
  });

  axiosInstance()
    .post("/feed", {
      feed: {
        date: date,
        name: name,
        quantity: quantity,
        price: price,
      },
    })
    .then((res) => {
      console.log("RESPONSE", res);

      dispatch({
        type: ADD_FEED_LOAD_SUCCESS,
        payload: res.data,
      });
    })
    .catch((err) => {
      console.log("ERROR", err);

      dispatch({
        type: ADD_FEED_LOAD_ERROR,
        payload: err.response ? err.response.data : CONNECTION_ERROR,
      });
    });
};
