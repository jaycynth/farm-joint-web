import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import RegisterUI from "../../layouts/Register";
import useForm from "./useForm";

//logic for the  register ui
function RegisterContainer() {
  useEffect(() => {}, []);
  return <RegisterUI form={useForm()} />;
}

export default RegisterContainer;
