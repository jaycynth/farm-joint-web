import { CLEAR_ADD_SUPPLEMENT } from "../../../constants/actionTypes";

export default () => (dispatch) => {
  dispatch({
    type: CLEAR_ADD_SUPPLEMENT,
  });
};
