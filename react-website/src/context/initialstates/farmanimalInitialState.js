export default {
    farmanimal: {
        loading: false,
        error: null,
        data: [],
        isSearchActive: false,
        foundContacts: [],
    },
    addFarmanimal: {
        loading: false,
        error: null,
        data: null,
    },
};