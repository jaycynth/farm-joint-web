/* eslint-disable import/no-anonymous-default-export */
import {
  PRODUCE_LOADING,
  PRODUCE_LOAD_ERROR,
  PRODUCE_LOAD_SUCCESS,
} from "../../../constants/actionTypes";
import { CONNECTION_ERROR } from "../../../constants/api";
import axiosInstance from "../../../helpers/axios";

export default (history) => (dispatch) => {
  dispatch({
    type: PRODUCE_LOADING,
  });
  axiosInstance(history)
    .get("/produce")
    .then((res) => {
      console.log("DATAA", res.data.produce_list)
      dispatch({
        type: PRODUCE_LOAD_SUCCESS,
        payload: res.data.produce_list,
      });
    })
    .catch((err) => {
      dispatch({
        type: PRODUCE_LOAD_ERROR,
        payload: err.response ? err.response.data : CONNECTION_ERROR,
      });
    });
};
