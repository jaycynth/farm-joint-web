/* eslint-disable import/no-anonymous-default-export */
export default {
    health: {
        loading: false,
        error: null,
        data: [],
        isSearchActive: false,
        foundContacts: [],
    },
    addHealth: {
        loading: false,
        error: null,
        data: null,
    },
};