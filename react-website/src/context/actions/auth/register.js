import {
  REGISTER_ERROR,
  REGISTER_LOADING,
  REGISTER_SUCCESS,
} from "../../../constants/actionTypes";
import axiosInstance from "../../../helpers/axios";

export const register = ({
  username: user_name,
  phonenumber: phone_number,
}) => (dispatch) => {
  dispatch({
    type: REGISTER_LOADING,
  });
  axiosInstance()
    .post("/users", {
      user: {
        user_name: user_name,
        phone_number: phone_number,
      },
    })
    .then((res) => {
      dispatch({
        type: REGISTER_SUCCESS,
        payload: res.data,
      });
    })
    .catch((err) => {
      dispatch({
        type: REGISTER_ERROR,
        payload: err.response.data,
      });
    });
};
