/* eslint-disable import/no-anonymous-default-export */
import {
  SUPPLEMENT_LOADING,
  SUPPLEMENT_LOAD_ERROR,
  SUPPLEMENT_LOAD_SUCCESS,
} from "../../../constants/actionTypes";
import { CONNECTION_ERROR } from "../../../constants/api";
import axiosInstance from "../../../helpers/axios";

export default (history) => (dispatch) => {
  dispatch({
    type: SUPPLEMENT_LOADING,
  });
  axiosInstance(history)
    .get("/supplement")
    .then((res) => {
      dispatch({
        type: SUPPLEMENT_LOAD_SUCCESS,
        payload: res.data.supplement_list,
      });
    })
    .catch((err) => {
      dispatch({
        type: SUPPLEMENT_LOAD_ERROR,
        payload: err.response ? err.response.data : CONNECTION_ERROR,
      });
    });
};
