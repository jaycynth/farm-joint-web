/* eslint-disable import/no-anonymous-default-export */
export default {
    purchase: {
        loading: false,
        error: null,
        data: [],
    },
    addPurchase: {
        loading: false,
        error: null,
        data: null,
    },
    filterByDate: {
        loading: false,
        error: null,
        data: [],
    }
};