import { CLEAR_AUTH_DATA } from "../../../constants/actionTypes";

export default () => (dispatch) => {
  dispatch({
    type: CLEAR_AUTH_DATA,
  });
};
