import { CLEAR_ADD_FEED } from "../../../constants/actionTypes";

export default () => (dispatch) => {
  dispatch({
    type: CLEAR_ADD_FEED,
  });
};
