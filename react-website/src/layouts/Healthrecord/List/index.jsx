import React, { useContext } from "react";
import Header from "../../../components/Header";
import { Link } from "react-router-dom";
import {
  Button,
  Placeholder,
  Container,
  Table,
  Menu,
  Icon,
  Input,
} from "semantic-ui-react";
import { GlobalContext } from "../../../context/Provider";

import "./index.css";
import searchHealth from "../../../context/actions/health/searchHealth";

const HealthrecordUI = ({
  state: {
    health: { loading, error, data, isSearchActive, foundContacts },
  },
}) => {
  const { healthDispatch: dispatch } = useContext(GlobalContext);

  const onChange = (e, { value }) => {
    const searchText = value.trim().replace(/" "/g, "");
    searchHealth(searchText)(dispatch);
  };

  const BarStyling = {
    width: "20rem",
    background: "#F2F1F9",
    border: "none",
    padding: "0.5rem",
  };

  return (
    <div>
      <Header />
      <Container className="health-container">
        {loading && (
          <>
            {""}
            <Placeholder>
              <Placeholder.Header image>
                <Placeholder.Line />
                <Placeholder.Line />
              </Placeholder.Header>
              <Placeholder.Paragraph>
                <Placeholder.Line />
                <Placeholder.Line />
                <Placeholder.Line />
                <Placeholder.Line />
              </Placeholder.Paragraph>
            </Placeholder>
          </>
        )}

        <Menu secondary>
          <Menu.Item>
            <Button as={Link} to="/add/health" primary basic icon>
              <Icon name="add"></Icon>
              Add Health Record
            </Button>
          </Menu.Item>

          <Menu.Menu position="right">
            <Menu.Item>
              <Input
                style={BarStyling}
                icon="search"
                onChange={onChange}
                placeholder="Search by health procedure type"
              />
            </Menu.Item>
          </Menu.Menu>
        </Menu>
        <h4>Health Records</h4>
        <Table singleLine>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell>Date</Table.HeaderCell>
              <Table.HeaderCell>Procedure Type</Table.HeaderCell>
              <Table.HeaderCell>Description</Table.HeaderCell>
              <Table.HeaderCell>Vet Name</Table.HeaderCell>
            </Table.Row>
          </Table.Header>

          <Table.Body className="health-table-body"></Table.Body>
          <Table.Body className="health-table-body">
            {isSearchActive
              ? foundContacts?.length > 0 &&
                foundContacts.map((health) => (
                  <Table.Row key={health.health_id}>
                    <Table.Cell>{health.date}</Table.Cell>
                    <Table.Cell>{health.procedure_type}</Table.Cell>
                    <Table.Cell>{health.description}</Table.Cell>
                    <Table.Cell>{health.vet_name}</Table.Cell>
                  </Table.Row>
                ))
              : data?.length > 0 &&
                data.map((health) => (
                  <Table.Row key={health.health_id}>
                    <Table.Cell>{health.date}</Table.Cell>
                    <Table.Cell>{health.procedure_type}</Table.Cell>
                    <Table.Cell>{health.description}</Table.Cell>
                    <Table.Cell>{health.vet_name}</Table.Cell>
                  </Table.Row>
                ))}
          </Table.Body>
        </Table>
      </Container>
    </div>
  );
};

export default HealthrecordUI;
