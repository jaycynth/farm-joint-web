import React, { useContext, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { GlobalContext } from "../../../context/Provider";
import FarmanimalListUI from "../../../layouts/Farmanimal/List";
import getFarmanimal from "../../../context/actions/farmanimal/getFarmanimal";

const FarmanimalContainer = () => {
  const { farmanimalDispatch, farmanimalState } = useContext(GlobalContext);
  const history = useHistory();

  console.log("farmanimalState", farmanimalState);

  useEffect(() => {
    getFarmanimal(history)(farmanimalDispatch);
  }, [farmanimalDispatch, history]);
  return <FarmanimalListUI state={farmanimalState} />;
};

export default FarmanimalContainer;
