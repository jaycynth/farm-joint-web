import React from "react";
import Header from "../../../components/Header";
import {
  Grid,
  Header as SemanticHeader,
  Card,
  Form,
  Button,
  Select,
} from "semantic-ui-react";

import { Prompt } from "react-router-dom";

import "./index.css";
import types from "../../../utils/types";

const CreateProduceUI = ({
  loading,
  onChange,
  formIsHalfFilled,
  formInvalid,
  onSubmit,
}) => {
  return (
    <div>
      <Prompt
        when={formIsHalfFilled}
        message={JSON.stringify({
          header: "confirm",
          content: "You have unsavd changes; You sure you want to leave?",
        })}
      />
      <Header />
      <Grid centered>
        <Grid.Column className="form-column">
          <SemanticHeader>Add a new produce</SemanticHeader>
          <Card fluid>
            <Card.Content>
              <Form unstackable>
                <Form.Input
                  label="Date"
                  placeholder="Date"
                  name="date"
                  onChange={onChange}
                />
                <Form.Input
                  label="Produce Type"
                  placeholder="Produce Type"
                  name="producetype"
                  onChange={onChange}
                />
                <Form.Group widths={2}>
                  <Form.Input
                    label="Quantity"
                    placeholder="Quantity"
                    name="quantity"
                    onChange={onChange}
                  />
                  <Form.Input
                    label="Quantity Type"
                    placeholder="Quantity Type"
                    name="quantitytype"
                    control={Select}
                    options={types}
                    onChange={onChange}
                  />
                </Form.Group>
                <Form.Input
                  label="Each Price"
                  placeholder="Each Price"
                  name="eachprice"
                  onChange={onChange}
                />
                <Form.Input
                  label="Farm Animal"
                  placeholder="Farm Animal"
                  name="farmanimal"
                  onChange={onChange}
                />
                <Button
                  primary
                  fluid
                  disabled={formInvalid || loading}
                  onClick={onSubmit}
                  loading={loading}
                  type="submit"
                >
                  Submit
                </Button>
              </Form>
            </Card.Content>
          </Card>
        </Grid.Column>
      </Grid>
    </div>
  );
};

export default CreateProduceUI;
