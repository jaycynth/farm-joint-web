import React, { useContext, useEffect } from "react";
import { useHistory } from "react-router-dom";
import getHealthRecords from "../../../context/actions/health/getHealthRecords";
import { GlobalContext } from "../../../context/Provider";
import HealthrecordUI from "../../../layouts/Healthrecord/List";
import {useLocation} from "react-router-dom";



const HealthrecordsContainer = () => {
  const { healthDispatch, healthState } = useContext(GlobalContext);
  const history = useHistory();

  console.log("healthState", healthState);

  const healthrecord = useLocation().search;
  const farmanimalId = new URLSearchParams(healthrecord).get('farmanimal_id');

  localStorage.setItem("FARMANIMALID", JSON.stringify(farmanimalId))




  useEffect(() => {
    getHealthRecords(history)(healthDispatch);
  }, [farmanimalId, healthDispatch, history]);

  return <HealthrecordUI  state={healthState}/>;
};

export default HealthrecordsContainer;
