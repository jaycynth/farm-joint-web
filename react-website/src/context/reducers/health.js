import {
  HEALTH_LOADING,
  HEALTH_LOAD_SUCCESS,
  HEALTH_LOAD_ERROR,
  ADD_HEALTH_LOADING,
  ADD_HEALTH_LOAD_ERROR,
  ADD_HEALTH_LOAD_SUCCESS,
  CLEAR_ADD_HEALTH,
  SEARCH_HEALTH,
} from "../../constants/actionTypes";

const Health = (state, { payload, type }) => {
  switch (type) {
    case SEARCH_HEALTH: {
      const searchValue = payload?.toLowerCase();
      return {
        ...state,
        health: {
          ...state.health,
          loading: false,
          isSearchActive: !!payload.length > 0 || false,
          foundContacts: state.health.data?.filter((item) => {
            try {
              console.log(
                "TAGG",
                item.procedure_type.toLowerCase().search(searchValue) !== -1
              );
              return item.procedure_type.toLowerCase().search(searchValue) !== -1;
            } catch (error) {
              console.log("TAGG", error);
              return [];
            }
          }),
        },
      };
    }
    case HEALTH_LOADING: {
      return {
        ...state,
        health: {
          ...state.health,
          loading: true,
        },
      };
    }

    case HEALTH_LOAD_SUCCESS: {
      return {
        ...state,
        health: {
          ...state.health,
          loading: false,
          data: payload,
        },
      };
    }

    case HEALTH_LOAD_ERROR: {
      return {
        ...state,
        health: {
          ...state.health,
          loading: false,
          error: payload,
        },
      };
    }
    case ADD_HEALTH_LOADING: {
      return {
        ...state,
        addHealth: {
          ...state.addHealth,
          error: null,
          loading: true,
        },
      };
    }
    case ADD_HEALTH_LOAD_SUCCESS: {
      return {
        ...state,
        addHealth: {
          ...state.addHealth,
          loading: false,
          data: payload,
        },
        health: {
          ...state.health,
          loading: false,
          data: [payload, ...state.health.data],
        },
      };
    }
    case ADD_HEALTH_LOAD_ERROR: {
      return {
        ...state,
        addHealth: {
          ...state.addHealth,
          loading: false,
        },
      };
    }

    case CLEAR_ADD_HEALTH: {
      return {
        ...state,
        addHealth: {
          ...state.addHealth,
          error: null,
          loading: false,
          data: null,
        },
      };
    }
    default:
      return state;
  }
};

export default Health;
