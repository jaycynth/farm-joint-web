/* eslint-disable import/no-anonymous-default-export */
export default [
  {
    text: "HEALTHY",
    value: "HEALTHY",
    key: "af",
  },
  {
    text: "SICKLY",
    value: "SICKLY",
    key: "ax",
  },
  {
    text: "OLD_AGED",
    value: "OLD_AGED",
    key: "ap",
  },
  {
    text: "PREGNANT",
    value: "PREGNANT",
    key: "ag",
  },
  {
    text: "EXPECTANT",
    value: "EXPECTANT",
    key: "an",
  },
];
