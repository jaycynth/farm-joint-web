import { SEARCH_SUPPLEMENT } from "../../../constants/actionTypes";

export default (searchText) => (dispatch) => {
    dispatch({
      type: SEARCH_SUPPLEMENT,
      payload: searchText,
    });
  };