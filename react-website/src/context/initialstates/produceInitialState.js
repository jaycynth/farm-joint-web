/* eslint-disable import/no-anonymous-default-export */
export default {
    produce: {
        loading: false,
        error: null,
        data: [],
        isSearchActive: false,
        foundContacts: [],
    },
    addProduce: {
        loading: false,
        error: null,
        data: null,
    },
    produceRate: {
        prodLoading: false,
        prodError: null,
        prodData: [],
    },
};