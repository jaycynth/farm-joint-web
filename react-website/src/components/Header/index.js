import React, { useContext } from "react";
import { Menu, Button, Icon, Image } from "semantic-ui-react";
import logo from "../../assets/logo.svg";
import { Link, useHistory, useLocation } from "react-router-dom";
import isAuthenticated from "../../utils/isAuthenticated";
import logout from "../../context/actions/auth/logout";
import { GlobalContext } from "../../context/Provider";

function Header() {
  const { pathname } = useLocation();
  console.log("location", pathname);

  const history = useHistory();

  const { produceDispatch: dispatch } = useContext(GlobalContext);

  const handleUserLogout = () => {
    logout(history)(dispatch);
  };

  return (
    <Menu secondary pointing>
      <Image src={logo} width={60} />
      <Menu.Item as={Link} to="/" style={{ fontSize: 24 }}>
        Farm Assist
      </Menu.Item>

      {isAuthenticated() && (
        <Menu.Item position="right">
          <Button as={Link} to="/statistics" primary basic icon>
            Stats Records
          </Button>
        </Menu.Item>
      )}

      {isAuthenticated() && (
        <Menu.Item>
          <Button as={Link} to="/produce" primary basic icon>
            Produce Records
          </Button>
        </Menu.Item>
      )}

      {isAuthenticated() && (
        <Menu.Item>
          <Button as={Link} to="/farmanimal" primary basic icon>
            Farm Animal Records
          </Button>
        </Menu.Item>
      )}
      {isAuthenticated() && (
        <Menu.Item>
          <Button as={Link} to="/feed" primary basic icon>
            Feed Records
          </Button>
        </Menu.Item>
      )}
      {isAuthenticated() && (
        <Menu.Item>
          <Button as={Link} to="/supplement" primary basic icon>
            Supplement Records
          </Button>
        </Menu.Item>
      )}
      {isAuthenticated() && (
        <Menu.Item>
          {" "}
          <Button
            onClick={handleUserLogout}
            color="red"
            circular="true"
            basic
            icon
          >
            <Icon name="log out"></Icon>
            Logout
          </Button>
        </Menu.Item>
      )}
    </Menu>
  );
}

export default Header;
