import React, { useContext } from "react";
import { GlobalContext } from "../../context/Provider";
import LoginUI from "../../layouts/Login";
import useForm from "./useForm";

//logic for the  register ui
const LoginContainer = () => {
  const {
    authState: {
      auth: { data },
    },
  } = useContext(GlobalContext);

  console.log("AUTHDATA", data)

  localStorage.setItem("USERNAME", JSON.stringify(data?.user?.user_name))



  return (
    /* <h1>{data ?`Welcome ${data?.user_name}` : "Login Here"}</h1>
            <Link to="/users">Register</Link> */
    <LoginUI form={useForm()} />
  );
};

export default LoginContainer;
