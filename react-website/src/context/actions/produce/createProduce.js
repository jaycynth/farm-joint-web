/* eslint-disable import/no-anonymous-default-export */
import {
  ADD_PRODUCE_LOADING,
  ADD_PRODUCE_LOAD_ERROR,
  ADD_PRODUCE_LOAD_SUCCESS,
} from "../../../constants/actionTypes";
import { CONNECTION_ERROR } from "../../../constants/api";
import axiosInstance from "../../../helpers/axios";

export default ({
  date,
  producetype: produce_type,
  quantity,
  eachprice: each_price,
  farmanimal: farmanimal_id,
  quantitytype: quantity_type,
}) => (dispatch) => {
  dispatch({
    type: ADD_PRODUCE_LOADING,
  });

  axiosInstance()
    .post("/produce", {
      produce: {
        date: date,
        produce_type: produce_type,
        quantity: quantity,
        each_price: each_price,
        farmanimal_id: farmanimal_id,
        quantity_type: quantity_type,
      },
    })
    .then((res) => {
      console.log("RESPONSE", res.data);

      dispatch({
        type: ADD_PRODUCE_LOAD_SUCCESS,
        payload: res.data.produce,
      });
    })
    .catch((err) => {
      console.log("ERROR", err);

      dispatch({
        type: ADD_PRODUCE_LOAD_ERROR,
        payload: err.response ? err.response.data : CONNECTION_ERROR,
      });
    });
};
