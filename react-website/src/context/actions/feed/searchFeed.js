/* eslint-disable import/no-anonymous-default-export */
import { SEARCH_FEED } from "../../../constants/actionTypes";

export default (searchText) => (dispatch) => {
    dispatch({
      type: SEARCH_FEED,
      payload: searchText,
    });
  };