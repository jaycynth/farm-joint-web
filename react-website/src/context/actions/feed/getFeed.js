/* eslint-disable import/no-anonymous-default-export */
import {
  FEED_LOADING,
  FEED_LOAD_ERROR,
  FEED_LOAD_SUCCESS,
} from "../../../constants/actionTypes";
import { CONNECTION_ERROR } from "../../../constants/api";
import axiosInstance from "../../../helpers/axios";

export default (history) => (dispatch) => {
  dispatch({
    type: FEED_LOADING,
  });
  axiosInstance(history)
    .get("/feed")
    .then((res) => {
      dispatch({
        type: FEED_LOAD_SUCCESS,
        payload: res.data.feed_list,
      });
    })
    .catch((err) => {
      dispatch({
        type: FEED_LOAD_ERROR,
        payload: err.response ? err.response.data : CONNECTION_ERROR,
      });
    });
};
