/* eslint-disable import/no-anonymous-default-export */
import { CLEAR_ADD_HEALTH } from "../../../constants/actionTypes";

export default () => (dispatch) => {
  dispatch({
    type: CLEAR_ADD_HEALTH,
  });
};
