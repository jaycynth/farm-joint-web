import React, { useContext, useEffect } from "react";
import { GlobalContext } from "../../context/Provider";
import StatisticsUI from "../../layouts/Statistics";
import getPurchasesRates from "../../context/actions/purchase/getPurchasesRates";
import filterByTimestamp from "../../context/actions/purchase/filterByTimestamp";

import produceRate from "../../context/actions/produce/produceRate";

const StatisticsContainer = () => {
    const { purchaseDispatch, purchaseState } = useContext(GlobalContext);
    const { produceDispatch, produceState } = useContext(GlobalContext);


    console.log("purchaseproduceState", produceState);

    const {
        purchase: { data },
    } = purchaseState;
    const {
        produceRate: { prodData },
    } = produceState;


    let start, end; 

 const startDateChange = (e) => {
    const dateStr = e.target.value;
    let startTimestamp = parseInt(new Date(dateStr).getTime() / 1000);
    start = startTimestamp

}

const endDateChange = (e) => {
  const dateStr = e.target.value
  let endTimestamp = parseInt(new Date(dateStr).getTime()/1000)

   end = endTimestamp

}



    const filterByDate = () => {
        filterByTimestamp(start, end)(purchaseDispatch);
        console.log("STARTEND", start, end)

    };

    useEffect(() => {
        if (data.length === 0) {
            getPurchasesRates()(purchaseDispatch);
        }
        if (prodData.length === 0) {
            produceRate()(produceDispatch);
        }
    }, [data.length, prodData.length, produceDispatch, purchaseDispatch]);

 


    return ( <StatisticsUI purchaseState = { purchaseState }
        produceState = { produceState }
        filterByDate = { filterByDate }
        startDateChange = {startDateChange}
        endDateChange = {endDateChange}
        />
    );
};

export default StatisticsContainer;