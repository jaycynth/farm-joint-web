/* eslint-disable import/no-anonymous-default-export */
import { CLEAR_ADD_PRODUCE } from "../../../constants/actionTypes";

export default () => (dispatch) => {
  dispatch({
    type: CLEAR_ADD_PRODUCE,
  });
};
