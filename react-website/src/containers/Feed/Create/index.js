import React, { useContext, useEffect, useState } from "react";
import createFeed from "../../../context/actions/feed/createFeed";
import { GlobalContext } from "../../../context/Provider";
import { useHistory } from "react-router-dom";
import clearCreateFeed from "../../../context/actions/feed/clearCreateFeed";
import CreateFeedUI from "../../../layouts/Feed/Create";

const CreateFeedContainer = () => {
  const [form, setForm] = useState({});
  const history = useHistory();

  const {
    feedDispatch,
    feedState: {
      addFeed: { loading, error, data },
    },
  } = useContext(GlobalContext);

  const formIsHalfFilled =
    Object.values(form).filter((item) => item !== "").length > 0 && !data;

  useEffect(() => {
    if (data) {
      history.push("/feed");
    }
    return () => {
      clearCreateFeed()(feedDispatch);
    };
  }, [data, feedDispatch, history]);

  const onChange = (e, { name, value }) => {
    setForm({ ...form, [name]: value });
  };

  console.log("form", form);

  const onSubmit = () => {
    createFeed(form)(feedDispatch);
  };

  const formInValid =
    !form.name?.length ||
    !form.date?.length ||
    !form.quantity?.length ||
    !form.price?.length;

  return (
    <CreateFeedUI
      onSubmit={onSubmit}
      formInvalid={formInValid}
      onChange={onChange}
      form={form}
      formIsHalfFilled={formIsHalfFilled}
      loading={loading}
    />
  );
};

export default CreateFeedContainer;
