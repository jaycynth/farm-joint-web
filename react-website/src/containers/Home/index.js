import React from "react";
import HomepageLayout from "../../layouts/Home/HomePageLayout";

const HomeContainer = () => {
  return <HomepageLayout />;
};

export default HomeContainer;
