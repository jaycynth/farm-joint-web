/* eslint-disable import/no-anonymous-default-export */
import {
    FILTER_PURCHASE_BY_DATE_ERROR,
    FILTER_PURCHASE_BY_DATE_SUCCESS,
    FILTER_PURCHASE_BY_DATE_LOADING,
} from "../../../constants/actionTypes";
import { CONNECTION_ERROR } from "../../../constants/api";
import axiosInstance from "../../../helpers/axios";

export default (starkt, enkd) => (dispatch) => {
    console.log("STARTEND2", starkt, enkd)

    dispatch({
        type: FILTER_PURCHASE_BY_DATE_LOADING,
    });
    axiosInstance()
        .post("/purchases/purchase_rate", {
            rate_filters:[{
                rate_id: "DAY",
                start_timestamp: starkt,
                end_timestamp: enkd
            }],

            item_types: ["FEED", "PRODUCE", "SUPPLEMENT"],
        })
        .then((res) => {
            console.log("RESPONSEFILTER", res);

            dispatch({
                type: FILTER_PURCHASE_BY_DATE_SUCCESS,
                payload: res.data,
            });
        })
        .catch((err) => {
            console.log("ERROR", err);

            dispatch({
                type: FILTER_PURCHASE_BY_DATE_ERROR,
                payload: err.response ? err.response.data : CONNECTION_ERROR,
            });
        });
};