import React from "react";
import Header from "../../../components/Header";
import {
  Grid,
  Header as SemanticHeader,
  Card,
  Form,
  Button,
} from "semantic-ui-react";

import { Prompt } from "react-router-dom";

import "./index.css";

const CreateFeedUI = ({
  loading,
  onChange,
  formIsHalfFilled,
  formInvalid,
  onSubmit,
}) => {
  return (
    <div>
      <Prompt
        when={formIsHalfFilled}
        message={JSON.stringify({
          header: "confirm",
          content: "You have unsavd changes; You sure you want to leave?",
        })}
      />
      <Header />
      <Grid centered>
        <Grid.Column className="form-column">
          <SemanticHeader>Add a new feed</SemanticHeader>
          <Card fluid>
            <Card.Content>
              <Form unstackable>
                <Form.Input
                  label="Date"
                  placeholder="Date"
                  name="date"
                  onChange={onChange}
                />
                <Form.Input
                  label="Name"
                  placeholder="Name"
                  name="name"
                  onChange={onChange}
                />
                <Form.Input
                  label="Quantity"
                  placeholder="Quantity"
                  name="quantity"
                  onChange={onChange}
                />

                <Form.Input
                  label="Price"
                  placeholder="Price"
                  name="price"
                  onChange={onChange}
                />

                <Button
                  primary
                  fluid
                  disabled={formInvalid || loading}
                  onClick={onSubmit}
                  loading={loading}
                  type="submit"
                >
                  Submit
                </Button>
              </Form>
            </Card.Content>
          </Card>
        </Grid.Column>
      </Grid>
    </div>
  );
};

export default CreateFeedUI;
