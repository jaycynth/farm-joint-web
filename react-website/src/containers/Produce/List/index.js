import React, { useContext, useEffect } from "react";
import { useHistory } from "react-router-dom";
import getProduce from "../../../context/actions/produce/getProduce";
import { GlobalContext } from "../../../context/Provider";
import ProduceListUI from "../../../layouts/Produce/ProduceList";

//logic for the  register ui
const ProduceContainer = () => {
  const { produceDispatch, produceState } = useContext(GlobalContext);
  const history = useHistory();

  console.log("produceState", produceState);

  const {
    produce: { data },
  } = produceState;

  useEffect(() => {
      getProduce(history)(produceDispatch);
    
  }, [history, produceDispatch]);
  return <ProduceListUI state={produceState} />;
};

export default ProduceContainer;
