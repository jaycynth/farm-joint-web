/* eslint-disable import/no-anonymous-default-export */
import { SEARCH_HEALTH } from "../../../constants/actionTypes";

export default (searchText) => (dispatch) => {
    dispatch({
      type: SEARCH_HEALTH,
      payload: searchText,
    });
  };