export default [
  {
    text: "KG",
    value: "KG",
    key: "af",
  },
  {
    text: "LITRE",
    value: "LITRE",
    key: "ax",
  },
  {
    text: "TRAY",
    value: "TRAY",
    key: "ag",
  },
];
