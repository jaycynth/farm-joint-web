import React, { useContext, useEffect, useState } from "react";
import createSupplement from "../../../context/actions/supplement/createSupplement";
import { GlobalContext } from "../../../context/Provider";
import { useHistory } from "react-router-dom";
import clearCreateSupplement from "../../../context/actions/supplement/clearCreateSupplement";
import CreateSupplementUI from "../../../layouts/Supplement/Create";

const CreateSupplementContainer = () => {
  const [form, setForm] = useState({});
  const history = useHistory();

  const {
    supplementDispatch,
    supplementState: {
      addSupplement: { loading, error, data },
    },
  } = useContext(GlobalContext);

  const formIsHalfFilled =
    Object.values(form).filter((item) => item !== "").length > 0 && !data;

  useEffect(() => {
    if (data) {
      history.push("/supplement");
    }
    return () => {
      clearCreateSupplement()(supplementDispatch);
    };
  }, [data, history, supplementDispatch]);

  const onChange = (e, { name, value }) => {
    setForm({ ...form, [name]: value });
  };

  console.log("form", form);

  const onSubmit = () => {
    createSupplement(form)(supplementDispatch);
  };

  const formInValid =
    !form.datebought?.length ||
    !form.name?.length ||
    !form.units?.length ||
    !form.quantity?.length ||
    !form.price?.length;

  return (
    <CreateSupplementUI
      onSubmit={onSubmit}
      formInvalid={formInValid}
      onChange={onChange}
      form={form}
      formIsHalfFilled={formIsHalfFilled}
      loading={loading}
    />
  );
};

export default CreateSupplementContainer;
