/* eslint-disable import/no-anonymous-default-export */
import {
  ADD_FARMANIMAL_LOADING,
  ADD_FARMANIMAL_LOAD_ERROR,
  ADD_FARMANIMAL_LOAD_SUCCESS,
} from "../../../constants/actionTypes";
import { CONNECTION_ERROR } from "../../../constants/api";
import axiosInstance from "../../../helpers/axios";

export default ({
  birthdate: birth_date,
  tag: tag,
  status: status,
  photo: photo,
  calfingcount: calfingCount,
}) => (dispatch) => {
  dispatch({
    type: ADD_FARMANIMAL_LOADING,
  });

  axiosInstance()
    .post("/farmanimal", {
      farmanimal: {
        birth_date: birth_date,
        tag: tag,
        status: status,
        photo: "photo",
        calfingCount: calfingCount,
      },
    })
    .then((res) => {
      console.log("RESPONSE", res);

      dispatch({
        type: ADD_FARMANIMAL_LOAD_SUCCESS,
        payload: res.data.farmanimal,
      });
    })
    .catch((err) => {
      console.log("ERROR", err);

      dispatch({
        type: ADD_FARMANIMAL_LOAD_ERROR,
        payload: err.response ? err.response.data : CONNECTION_ERROR,
      });
    });
};
