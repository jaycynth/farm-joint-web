/* eslint-disable import/no-anonymous-default-export */
export default {
    feed: {
        loading: false,
        error: null,
        data: [],
        isSearchActive: false,
        foundContacts: [],
    },
    addFeed: {
        loading: false,
        error: null,
        data: null,
    },
};