export default {
    supplement: {
        loading: false,
        error: null,
        data: [],
        isSearchActive: false,
        foundContacts: [],
    },
    addSupplement: {
        loading: false,
        error: null,
        data: null,
    },
};