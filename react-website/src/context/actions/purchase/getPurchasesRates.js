/* eslint-disable import/no-anonymous-default-export */
import {
  PURCHASE_LOADING,
  PURCHASE_LOAD_ERROR,
  PURCHASE_LOAD_SUCCESS,
} from "../../../constants/actionTypes";
import { CONNECTION_ERROR } from "../../../constants/api";
import axiosInstance from "../../../helpers/axios";

export default () => (dispatch) => {
  dispatch({
    type: PURCHASE_LOADING,
  });
  axiosInstance()
    .post("/purchases/purchase_rate", {
      item_types: ["FEED", "PRODUCE", "SUPPLEMENT"],
    })
    .then((res) => {
      console.log("RESPONSE", res);

      dispatch({
        type: PURCHASE_LOAD_SUCCESS,
        payload: res.data,
      });
    })
    .catch((err) => {
      console.log("ERROR", err);

      dispatch({
        type: PURCHASE_LOAD_ERROR,
        payload: err.response ? err.response.data : CONNECTION_ERROR,
      });
    });
};
