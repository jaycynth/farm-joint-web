/* eslint-disable import/no-anonymous-default-export */
import {
  ADD_SUPPLEMENT_LOADING,
  ADD_SUPPLEMENT_LOAD_ERROR,
  ADD_SUPPLEMENT_LOAD_SUCCESS,
} from "../../../constants/actionTypes";
import { CONNECTION_ERROR } from "../../../constants/api";
import axiosInstance from "../../../helpers/axios";

export default ({
  name: supplement_name,
  units: units,
  quantity: quantity,
  price: price,
  datebought: date_bought
}) => (dispatch) => {
  dispatch({
    type: ADD_SUPPLEMENT_LOADING,
  });

  axiosInstance()
    .post("/supplement", {
      supplement: {
        supplement_name: supplement_name,
        units: units,
        quantity: quantity,
        price: price,
        date_bought: date_bought
      },
    })
    .then((res) => {
      console.log("RESPONSE", res);

      dispatch({
        type: ADD_SUPPLEMENT_LOAD_SUCCESS,
        payload: res.data,
      });
    })
    .catch((err) => {
      console.log("ERROR", err);

      dispatch({
        type: ADD_SUPPLEMENT_LOAD_ERROR,
        payload: err.response ? err.response.data : CONNECTION_ERROR,
      });
    });
};
