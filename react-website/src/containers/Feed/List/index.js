import React, { useContext, useEffect } from "react";
import { useHistory } from "react-router-dom";
import getFeed from "../../../context/actions/feed/getFeed";
import { GlobalContext } from "../../../context/Provider";
import FeedListUI from "../../../layouts/Feed/List";

const FeedContainer = () => {
  const { feedDispatch, feedState } = useContext(GlobalContext);
  const history = useHistory();

  console.log("feedState", feedState);

  useEffect(() => {
    getFeed(history)(feedDispatch);
  }, [feedDispatch, history]);
  return <FeedListUI state={feedState} />;
};

export default FeedContainer;
