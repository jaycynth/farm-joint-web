import React, {useContext} from "react";
import Header from "../../../components/Header";

import { Link } from "react-router-dom";
import searchSupplement from "../../../context/actions/supplement/searchSupplement"
import { GlobalContext } from "../../../context/Provider";


import {
  Placeholder,
  Container,
  Table,
  Menu,
  Icon,
  Button,
  Input,
} from "semantic-ui-react";

const SupplementListUI = ({
  state: {
    supplement: { loading, error, data,isSearchActive, foundContacts },
  },
}) => {
  const { supplementDispatch: dispatch } = useContext(GlobalContext);


  const onChange = (e, { value }) => {
    const searchText = value.trim().replace(/" "/g, "");
    searchSupplement(searchText)(dispatch);
  };

  const BarStyling = {
    width: "20rem",
    background: "#F2F1F9",
    border: "none",
    padding: "0.5rem",
  };
  


  return (
    <div>
      <Header />
      <Container className="supplement-container">
        {loading && (
          <>
            {""}
            <Placeholder>
              <Placeholder.Header image>
                <Placeholder.Line />
                <Placeholder.Line />
              </Placeholder.Header>
              <Placeholder.Paragraph>
                <Placeholder.Line />
                <Placeholder.Line />
                <Placeholder.Line />
                <Placeholder.Line />
              </Placeholder.Paragraph>
            </Placeholder>
          </>
        )}
        <Menu secondary>
          <Menu.Item>
            <Button as={Link} to="/add/supplement" primary basic icon>
              <Icon name="add"></Icon>
              Add Supplement
            </Button>
          </Menu.Item>
          <Menu.Menu position="right">
            <Menu.Item>
              <Input icon="search"                
               style={BarStyling}
               onChange={onChange} placeholder="Search..." />
            </Menu.Item>
          </Menu.Menu>
        </Menu>
        <h4>Supplement Records</h4>
        <Table singleLine>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell>Date Bought</Table.HeaderCell>
              <Table.HeaderCell>Name</Table.HeaderCell>
              <Table.HeaderCell>Units</Table.HeaderCell>
              <Table.HeaderCell>Quantity</Table.HeaderCell>
              <Table.HeaderCell>Price</Table.HeaderCell>
            </Table.Row>
          </Table.Header>

          <Table.Body>
            {isSearchActive ? foundContacts?.length > 0 &&
              foundContacts.map((supplement) => (
                <Table.Row key={supplement.supplement_id}>
                  <Table.Cell>{supplement.date_bought}</Table.Cell>
                  <Table.Cell>{supplement.supplement_name}</Table.Cell>
                  <Table.Cell>{supplement.units}</Table.Cell>
                  <Table.Cell>{supplement.quantity}</Table.Cell>
                  <Table.Cell>{supplement.price}</Table.Cell>
                </Table.Row>
              )):
              data?.length > 0 &&
              data.map((supplement) => (
                <Table.Row key={supplement.supplement_id}>
                  <Table.Cell>{supplement.date_bought}</Table.Cell>
                  <Table.Cell>{supplement.supplement_name}</Table.Cell>
                  <Table.Cell>{supplement.units}</Table.Cell>
                  <Table.Cell>{supplement.quantity}</Table.Cell>
                  <Table.Cell>{supplement.price}</Table.Cell>
                </Table.Row>
              ))}
          </Table.Body>
        </Table>
      </Container>
    </div>
  );
};

export default SupplementListUI;
