/* eslint-disable import/no-anonymous-default-export */
import { useState, useContext, useEffect } from "react";
import { GlobalContext } from "../../context/Provider";
import { useHistory } from "react-router-dom";
import { login } from "../../context/actions/auth/login";
import clearAuthData from "../../context/actions/auth/clearAuthData";

export default () => {
  const [form, setForm] = useState({});
  const history = useHistory();

  const {
    authDispatch,
    authState: {
      auth: { loading, error, data },
    },
  } = useContext(GlobalContext);


  const onChange = (e, { name, value }) => {
    setForm({ ...form, [name]: value });
  };

  const loginFormValid = !form.username?.length || !form.phonenumber?.length;

  const onSubmit = () => {
    login(form)(authDispatch);
  };

  useEffect(() => {
    if (data) {
      if (data.user) {
        history.push("/statistics");
        window.location.reload();

      }
    }
    clearAuthData()(authDispatch);
  }, [authDispatch, data, history]);


  return { form, onChange, loading, error, loginFormValid, onSubmit };
};
