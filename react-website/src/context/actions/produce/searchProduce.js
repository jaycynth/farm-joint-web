/* eslint-disable import/no-anonymous-default-export */
import { SEARCH_PRODUCE } from "../../../constants/actionTypes";

export default (searchText) => (dispatch) => {
  dispatch({
    type: SEARCH_PRODUCE,
    payload: searchText,
  });
};
