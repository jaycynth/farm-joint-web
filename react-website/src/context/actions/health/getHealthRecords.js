/* eslint-disable import/no-anonymous-default-export */
import {
  HEALTH_LOADING,
  HEALTH_LOAD_ERROR,
  HEALTH_LOAD_SUCCESS,
} from "../../../constants/actionTypes";
import { CONNECTION_ERROR } from "../../../constants/api";
import axiosInstance from "../../../helpers/axios";

export default (history) => (dispatch) => {

  const farmanimalId = JSON.parse(localStorage.getItem("FARMANIMALID"))

  console.log("farmanimalId", farmanimalId);

  dispatch({
    type: HEALTH_LOADING,
  });
  axiosInstance(history)
    .get("/health?farmanimal_id="+ farmanimalId)
    .then((res) => {
      dispatch({
        type: HEALTH_LOAD_SUCCESS,
        payload: res.data.health_list,
      });
    })
    .catch((err) => {
      dispatch({
        type: HEALTH_LOAD_ERROR,
        payload: err.response ? err.response.data : CONNECTION_ERROR,
      });
    });
};
