/* eslint-disable import/no-anonymous-default-export */
import { CLEAR_ADD_FARM_ANIMAL } from "../../../constants/actionTypes";

export default () => (dispatch) => {
  dispatch({
    type: CLEAR_ADD_FARM_ANIMAL,
  });
};
