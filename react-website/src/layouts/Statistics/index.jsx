import React from "react";
import { Chart } from "react-google-charts";
import Header from "../../components/Header";
import { Container, Placeholder, Grid, Table, Button } from "semantic-ui-react";

import "./index.css";

import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    flexWrap: "wrap",
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 200,
  },
}));

const StatisticsUI = ({
  filterByDate,
  purchaseState: {
    purchase: { loading, error, data },
  },
  produceState: {
    produceRate: { prodLoading, prodError, prodData },
  },
  startDateChange,
  endDateChange,
}) => {
  const classes = useStyles();

  let data2 = JSON.parse(localStorage.getItem("DATALOAD"));

  const arrayData = () => {
    var arry = [];
    var dp = ["Months", "Production rate"];
    arry.push(dp);

    data2.produce_rate?.length > 0 &&
      data2.produce_rate.map((rate) =>
        arry.push([rate.rate_id, parseInt(rate.total_cost)])
      );

    return arry;
  };

  let produceCount = 0,
    supplementCount = 0,
    feedCount = 0,
    feedCost = 0,
    supplementCost = 0,
    produceCost = 0;

  return (
    <div>
      <Header />

      <Container className="statistics-container">
        <h2 className="title-header" style={{ "font-family": "Lora-Bold" }}>
          {`Welcome ${JSON.parse(localStorage.getItem("USERNAME"))}`}
        </h2>
        <h2>Statistics</h2>
        <h4 style={{ "font-family": "Lora" }}>
          Purchase Records, View where you spend more
        </h4>

        <Grid>
          {/* <Grid.Row className="grid-container">
            <div className="button-wrap">
              <div className="form-wrapper">
                <p primary className="filter">
                  Filter purchase records by date :
                </p>

                <form noValidate className={classes.container}>
                  <TextField
                    className={classes.textField}
                    id="date"
                    label="Start Date"
                    type="date"
                    defaultValue="2021-05-24"
                    onChange={startDateChange}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                  <TextField
                    id="date"
                    className={classes.textField}
                    label="End Date"
                    type="date"
                    onChange={endDateChange}
                    defaultValue="2021-05-24"
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />

                </form>
                <Button size="tiny" style={{"marginTop": "20px"}} className="filter-button" primary onClick={filterByDate}>Filter</Button>

              </div>
            </div>
          </Grid.Row> */}

          <Grid.Row className="grid-container">
            {loading && (
              <>
                {""}
                <Placeholder>
                  <Placeholder.Header image>
                    <Placeholder.Line />
                    <Placeholder.Line />
                  </Placeholder.Header>
                  <Placeholder.Paragraph>
                    <Placeholder.Line />
                    <Placeholder.Line />
                    <Placeholder.Line />
                    <Placeholder.Line />
                  </Placeholder.Paragraph>
                </Placeholder>
              </>
            )}
            {data.purchase_rates?.length > 0 &&
              data.purchase_rates.map((rates) =>
                // eslint-disable-next-line array-callback-return
                rates.items_stat.map((types) => {
                  if (types.item_type === "PRODUCE") {
                    produceCount = types.item_count;
                    produceCost = types.item_cost;
                  } else if (types.item_type === "FEED") {
                    feedCount = types.item_count;
                    feedCost = types.item_cost;
                  } else if (types.item_type === "SUPPLEMENT") {
                    supplementCount = types.item_count;
                    supplementCost = types.item_cost;
                  }
                })
              )}

            <Chart
              className="purchase-chart"
              width={"100%"}
              height={"300px"}
              chartType="PieChart"
              loader={<div>Loading Chart</div>}
              data={[
                ["Item Type", "Percentage bought"],
                ["PRODUCE", produceCount],
                ["FEED", feedCount],
                ["SUPPLEMENT", supplementCount],
              ]}
              options={{
                title: "Purchase Records",
                is3D: true,
              }}
              rootProps={{ "data-testid": "2" }}
            />
          </Grid.Row>

          <Grid.Row>
            <Table basic="very" singleLine className="statistics-table">
              <Table.Header>
                <Table.Row>
                  <Table.HeaderCell>Item Type</Table.HeaderCell>
                  <Table.HeaderCell>Overall Total Cost (Ksh)</Table.HeaderCell>
                </Table.Row>
              </Table.Header>
              <Table.Body>
                <Table.Row>
                  <Table.Cell>Feed</Table.Cell>
                  <Table.Cell>{feedCost}</Table.Cell>
                </Table.Row>
                <Table.Row>
                  <Table.Cell>Produce</Table.Cell>
                  <Table.Cell>{produceCost}</Table.Cell>
                </Table.Row>
                <Table.Row>
                  <Table.Cell>Supplement</Table.Cell>
                  <Table.Cell>{supplementCost}</Table.Cell>
                </Table.Row>
              </Table.Body>
            </Table>
          </Grid.Row>

          {prodLoading && (
            <>
              {""}
              <Placeholder>
                <Placeholder.Header image>
                  <Placeholder.Line />
                  <Placeholder.Line />
                </Placeholder.Header>
                <Placeholder.Paragraph>
                  <Placeholder.Line />
                  <Placeholder.Line />
                  <Placeholder.Line />
                  <Placeholder.Line />
                </Placeholder.Paragraph>
              </Placeholder>
            </>
          )}

          <Grid.Row>
            <h4 style={{ "font-family": "Lora" }}>
              Amount earned with production in the last six months
            </h4>

            {prodLoading && (
              <>
                {""}
                <Placeholder>
                  <Placeholder.Header image>
                    <Placeholder.Line />
                    <Placeholder.Line />
                  </Placeholder.Header>
                  <Placeholder.Paragraph>
                    <Placeholder.Line />
                    <Placeholder.Line />
                    <Placeholder.Line />
                    <Placeholder.Line />
                  </Placeholder.Paragraph>
                </Placeholder>
              </>
            )}

            <Chart
              width={"100%"}
              height={"400px"}
              chartType="LineChart"
              loader={<div>Loading Chart</div>}
              data={arrayData()}
              options={{
                hAxis: {
                  title: "Months",
                },
                vAxis: {
                  title: "Amount Earned(Ksh)",
                },
              }}
              rootProps={{ "data-testid": "1" }}
            />
          </Grid.Row>
        </Grid>
      </Container>
    </div>
  );
};

export default StatisticsUI;
