import React from "react";
import {
  Form,
  Button,
  Grid,
  Header as SemanticHeader,
  Segment,
} from "semantic-ui-react";
import Header from "../../components/Header";
import { Link } from "react-router-dom";

const RegisterUI = ({
  form: { onChange, loading, fieldErrors, form, registerFormValid, onSubmit },
}) => {
  return (
    <div>
      <Header />

      <Grid centered>
        <Grid.Column style={{ maxWidth: 550, marginTop: 20 }}>
          <SemanticHeader>Sign Up Here</SemanticHeader>
          <Segment>
            <Form>
              <Form.Field>
                <Form.Input
                  value={form.username || ""}
                  onChange={onChange}
                  name="username"
                  placeholder="User Name"
                  label="User Name"
                  error={
                    fieldErrors.user_name && {
                      content: fieldErrors.user_name,
                      pointing: "below",
                    }
                  }
                />
              </Form.Field>
              <Form.Field>
                <Form.Input
                  value={form.phonenumber || ""}
                  onChange={onChange}
                  name="phonenumber"
                  placeholder="Phone Number"
                  label="Phone Number"
                  error={
                    fieldErrors.phone_number && {
                      content: fieldErrors.phone_number,
                      pointing: "below",
                    }
                  }
                />
              </Form.Field>
              <Form.Field></Form.Field>
              <Button
                loading={loading}
                onClick={onSubmit}
                disabled={registerFormValid || loading}
                fluid
                primary
                type="submit"
              >
                Submit
              </Button>

              <Segment>
                Already have an account? <Link to="/auth/login">Login</Link>{" "}
              </Segment>
            </Form>
          </Segment>
        </Grid.Column>
      </Grid>
    </div>
  );
};

export default RegisterUI;
