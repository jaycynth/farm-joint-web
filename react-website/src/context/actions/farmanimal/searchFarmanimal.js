/* eslint-disable import/no-anonymous-default-export */
import { SEARCH_FARM_ANIMAL } from "../../../constants/actionTypes";

export default (searchText) => (dispatch) => {
    dispatch({
      type: SEARCH_FARM_ANIMAL,
      payload: searchText,
    });
  };