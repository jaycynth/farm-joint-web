import {
  SUPPLEMENT_LOADING,
  SUPPLEMENT_LOAD_SUCCESS,
  SUPPLEMENT_LOAD_ERROR,
  ADD_SUPPLEMENT_LOADING,
  ADD_SUPPLEMENT_LOAD_SUCCESS,
  ADD_SUPPLEMENT_LOAD_ERROR,
  CLEAR_ADD_SUPPLEMENT,
  SEARCH_SUPPLEMENT,
} from "../../constants/actionTypes";
const supplement = (state, { payload, type }) => {
  switch (type) {
    case SEARCH_SUPPLEMENT:
      {
          const searchValue = payload?.toLowerCase();
          return {
              ...state,
              supplement: {
                  ...state.supplement,
                  loading: false,
                  isSearchActive: !!payload.length > 0 || false,
                  foundContacts: state.supplement.data?.filter((item) => {
                      try {
                          console.log(
                              "TAGG",
                              item.supplement_name.toLowerCase().search(searchValue) !== -1
                          );
                          return item.supplement_name.toLowerCase().search(searchValue) !== -1;
                      } catch (error) {
                          console.log("TAGG", error);
                          return [];
                      }
                  }),
              },
          };
      }
    case SUPPLEMENT_LOADING: {
      return {
        ...state,
        supplement: {
          ...state.supplement,
          loading: true,
        },
      };
    }

    case SUPPLEMENT_LOAD_SUCCESS: {
      return {
        ...state,
        supplement: {
          ...state.supplement,
          loading: false,
          data: payload,
        },
      };
    }

    case SUPPLEMENT_LOAD_ERROR: {
      return {
        ...state,
        supplement: {
          ...state.supplement,
          loading: false,
          error: payload,
        },
      };
    }
    case ADD_SUPPLEMENT_LOADING: {
      return {
        ...state,
        addSupplement: {
          ...state.addSupplement,
          error: null,
          loading: true,
        },
      };
    }
    case ADD_SUPPLEMENT_LOAD_SUCCESS: {
      return {
        ...state,
        addSupplement: {
          ...state.addSupplement,
          loading: false,
          data: payload,
        },
        supplement: {
          ...state.supplement,
          loading: false,
          data: [payload, ...state.supplement.data],
        },
      };
    }
    case ADD_SUPPLEMENT_LOAD_ERROR: {
      return {
        ...state,
        addSupplement: {
          ...state.addSupplement,
          loading: false,
        },
      };
    }

    case CLEAR_ADD_SUPPLEMENT: {
      return {
        ...state,
        addSupplement: {
          ...state.addSupplement,
          error: null,
          loading: false,
          data: null,
        },
      };
    }
    default:
      return state;
  }
};

export default supplement;
