import {
  ADD_PRODUCE_LOADING,
  ADD_PRODUCE_LOAD_ERROR,
  ADD_PRODUCE_LOAD_SUCCESS,
  CLEAR_ADD_PRODUCE,
  LOGOUT_USER,
  PRODUCE_LOADING,
  PRODUCE_LOAD_ERROR,
  PRODUCE_LOAD_SUCCESS,
  SEARCH_PRODUCE,
  PRODUCE_RATE_LOADING,
  PRODUCE_RATE_LOAD_ERROR,
  PRODUCE_RATE_LOAD_SUCCESS,
} from "../../constants/actionTypes";
import produceInitialState from "../initialstates/produceInitialState";

const produce = (state, { payload, type }) => {
  switch (type) {
    case PRODUCE_LOADING: {
      return {
        ...state,
        produce: {
          ...state.produce,
          loading: true,
        },
      };
    }

    case PRODUCE_LOAD_SUCCESS: {
      return {
        ...state,
        produce: {
          ...state.produce,
          loading: false,
          data: payload,
        },
      };
    }

    case PRODUCE_LOAD_ERROR: {
      return {
        ...state,
        produce: {
          ...state.produce,
          loading: false,
          error: payload,
        },
      };
    }

    case PRODUCE_RATE_LOADING: {
      return {
        ...state,
        produceRate: {
          ...state.produceRate,
          prodLoading: true,
        },
      };
    }

    case PRODUCE_RATE_LOAD_SUCCESS: {
      return {
        ...state,
        produceRate: {
          ...state.produceRate,
          prodLoading: false,
          prodData: payload,
        },
      };
    }

    case PRODUCE_RATE_LOAD_ERROR: {
      return {
        ...state,
        produceRate: {
          ...state.produceRate,
          prodLoading: false,
          prodError: payload,
        },
      };
    }

    case LOGOUT_USER: {
      return {
        ...state,
        produceInitialState,
      };
    }
    case ADD_PRODUCE_LOADING: {
      return {
        ...state,
        addProduce: {
          ...state.addProduce,
          error: null,
          loading: true,
        },
      };
    }
    case ADD_PRODUCE_LOAD_SUCCESS: {
      return {
        ...state,
        addProduce: {
          ...state.addProduce,
          loading: false,
          data: payload,
        },
        produce: {
          ...state.produce,
          loading: false,
          data: [payload, ...state.produce.data],
        },
      };
    }
    case ADD_PRODUCE_LOAD_ERROR: {
      return {
        ...state,
        addProduce: {
          ...state.addProduce,
          loading: false,
        },
      };
    }

    case SEARCH_PRODUCE: {
      const searchValue = payload?.toLowerCase();
      return {
        ...state,
        produce: {
          ...state.produce,
          loading: false,
          isSearchActive: !!payload.length > 0 || false,
          foundContacts: state.produce.data?.filter((item) => {
            try {
              console.log(
                "TAGG",
                item.produce_type.toLowerCase().search(searchValue) !== -1
              );
              return item.produce_type.toLowerCase().search(searchValue) !== -1;
            } catch (error) {
              console.log("TAGG", error);
              return [];
            }
          }),
        },
      };
    }

    case CLEAR_ADD_PRODUCE: {
      return {
        ...state,
        addProduce: {
          ...state.addProduce,
          error: null,
          loading: false,
          data: null,
        },
      };
    }
    default:
      return state;
  }
};

export default produce;
