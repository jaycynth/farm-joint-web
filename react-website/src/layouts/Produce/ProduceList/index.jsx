import React, { useContext } from "react";
import { Link } from "react-router-dom";
import {
  Placeholder,
  Container,
  Table,
  Menu,
  Icon,
  Button,
  Input,
} from "semantic-ui-react";
import Header from "../../../components/Header";
import searchProduce from "../../../context/actions/produce/searchProduce";
import { GlobalContext } from "../../../context/Provider";

import "./index.css";

const ProduceListUI = ({
  state: {
    produce: { loading, error, data, isSearchActive, foundContacts },
  },
}) => {
  
  const { produceDispatch: dispatch } = useContext(GlobalContext);


  const onChange = (e, { value }) => {
    const searchText = value.trim().replace(/" "/g, "");
    searchProduce(searchText)(dispatch);
  };


  const BarStyling = {
    width: "20rem",
    background: "#F2F1F9",
    border: "none",
    padding: "0.5rem",
  };

  return (
    <div>
      <Header />

      <Container className="produce-container">
        {loading && (
          <>
            {""}
            <Placeholder>
              <Placeholder.Header image>
                <Placeholder.Line />
                <Placeholder.Line />
              </Placeholder.Header>
              <Placeholder.Paragraph>
                <Placeholder.Line />
                <Placeholder.Line />
                <Placeholder.Line />
                <Placeholder.Line />
              </Placeholder.Paragraph>
            </Placeholder>
          </>
        )}
        <Menu secondary>
          <Menu.Item>
            <Button as={Link} to="/add/produce" primary basic icon>
              <Icon name="add"></Icon>
              Add Produce
            </Button>
          </Menu.Item>

          <Menu.Menu position="right">
            <Menu.Item>
              <Input
                style={BarStyling}
                icon="search"
                onChange={onChange}
                placeholder="Search by produce type"
              />
            </Menu.Item>
          </Menu.Menu>
        </Menu>
        <h4>All Produce Records</h4>
        <Table singleLine>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell>Date</Table.HeaderCell>
              <Table.HeaderCell>Produce Type</Table.HeaderCell>
              <Table.HeaderCell>Quantity</Table.HeaderCell>
              <Table.HeaderCell>Quantity Type</Table.HeaderCell>
              <Table.HeaderCell> Each Price</Table.HeaderCell>
            </Table.Row>
          </Table.Header>

       

          <Table.Body className="produce-table-body">
            {isSearchActive
              ? foundContacts?.length > 0 &&
                foundContacts.map((produce) => (
                  <Table.Row key={produce.produce_id}>
                    <Table.Cell>{produce.date}</Table.Cell>
                    <Table.Cell>{produce.produce_type}</Table.Cell>
                    <Table.Cell>{produce.quantity}</Table.Cell>
                    <Table.Cell>{produce.quantity_type}</Table.Cell>
                    <Table.Cell>{produce.each_price}</Table.Cell>
                  
                  </Table.Row>
                ))
              : data?.length > 0 &&
                data.map((produce) => (
                  <Table.Row key={produce.produce_id}>
                    <Table.Cell>{produce.date}</Table.Cell>
                    <Table.Cell>{produce.produce_type}</Table.Cell>
                    <Table.Cell>{produce.quantity}</Table.Cell>
                    <Table.Cell>{produce.quantity_type}</Table.Cell>
                    <Table.Cell>{produce.each_price}</Table.Cell>
                   
                  </Table.Row>
                ))}
          </Table.Body>
        </Table>

        {/* <List>
          {data.produce_list?.length &&
            data.produce_list.map((produce) => (
              <List.Item key={produce.id}>
                <List.Content className="listContent">
                  <span className="spanItem">{produce.date}</span>
                  <span className="spanItem">{produce.produce_type}</span>
                  <span className="spanItem">{produce.each_price}</span>
                  <span className="spanItem">{produce.quantity}</span>
                  <span className="spanItem">{produce.quantity_type}</span>
                </List.Content>
              </List.Item>
            ))}
        </List> */}
      </Container>
    </div>
  );
};

export default ProduceListUI;
