import {
  PURCHASE_LOADING,
  PURCHASE_LOAD_ERROR,
  PURCHASE_LOAD_SUCCESS,
  FILTER_PURCHASE_BY_DATE_SUCCESS,
  FILTER_PURCHASE_BY_DATE_ERROR,
  FILTER_PURCHASE_BY_DATE_LOADING,
} from "../../constants/actionTypes";
const purchase = (state, { payload, type }) => {
  switch (type) {
    case PURCHASE_LOADING: {
      return {
        ...state,
        purchase: {
          ...state.purchase,
          loading: true,
        },
      };
    }

    case PURCHASE_LOAD_SUCCESS: {
      return {
        ...state,
        purchase: {
          ...state.purchase,
          loading: false,
          data: payload,
        },
      };
    }

    case PURCHASE_LOAD_ERROR: {
      return {
        ...state,
        purchase: {
          ...state.purchase,
          loading: false,
          error: payload,
        },
      };
    }

    case FILTER_PURCHASE_BY_DATE_LOADING: {
      return {
        ...state,
        filterByDate: {
          ...state.purchase,
          loading: true,
        },
      };
    }

    case FILTER_PURCHASE_BY_DATE_SUCCESS: {
      return {
        ...state,
        filterByDate: {
          ...state.filterByDate,
          loading: false,
          data: payload,
        },
      };
    }

    case FILTER_PURCHASE_BY_DATE_ERROR: {
      return {
        ...state,
        filterByDate: {
          ...state.filterByDate,
          loading: false,
          error: payload,
        },
      };
    }
    default:
      return state;
  }
};

export default purchase;
