import React from "react";
import {
  Form,
  Button,
  Grid,
  Message,
  Header as SemanticHeader,
  Segment,
} from "semantic-ui-react";
import { Link } from "react-router-dom";
import Header from "../../components/Header";

const LoginUI = ({
  form: { onChange, loading, error, form, loginFormValid, onSubmit },
}) => {

  return (
    <div>
      <Header />

      <Grid centered>
        <Grid.Column style={{ maxWidth: 550, marginTop: 20 }}>
          <SemanticHeader>Sign In Here</SemanticHeader>
          <Segment>
            <Form>
              {error && <Message content={error?.message} negative />}

              <Form.Field>
                <Form.Input
                  value={form.username || ""}
                  onChange={onChange}
                  name="username"
                  placeholder="User Name"
                  label="User Name"
                />
              </Form.Field>
              <Form.Field>
                <Form.Input
                  value={form.phonenumber || ""}
                  onChange={onChange}
                  name="phonenumber"
                  placeholder="Phone Number"
                  label="Phone Number"
                />
              </Form.Field>
              <Form.Field></Form.Field>
              <Button
                loading={loading}
                onClick={onSubmit}
                disabled={loginFormValid || loading}
                fluid
                primary
                type="submit"
              >
                Submit
              </Button>

              <Segment>
                Need an account.? <Link to="/auth/register">Register</Link>{" "}
              </Segment>
            </Form>
          </Segment>
        </Grid.Column>
      </Grid>
    </div>
  );
};

export default LoginUI;
