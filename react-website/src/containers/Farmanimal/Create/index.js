import React, { useContext, useEffect, useState } from "react";
import createFarmanimal from "../../../context/actions/farmanimal/createFarmanimal";
import { GlobalContext } from "../../../context/Provider";
import { useHistory } from "react-router-dom";
import clearCreateFarmanimal from "../../../context/actions/farmanimal/clearCreateFarmanimal";
import CreateFarmanimalUI from "../../../layouts/Farmanimal/Create";

const CreateFarmanimalContainer = () => {
  const [form, setForm] = useState({});
  const history = useHistory();

  const {
    farmanimalDispatch,
    farmanimalState: {
      addFarmanimal: { loading, error, data },
    },
  } = useContext(GlobalContext);

  const formIsHalfFilled =
    Object.values(form).filter((item) => item !== "").length > 0 && !data;

  useEffect(() => {
    if (data) {
      history.push("/farmanimal");
    }
    return () => {
      clearCreateFarmanimal()(farmanimalDispatch);
    };
  }, [data, farmanimalDispatch, history]);

  const onChange = (e, { name, value }) => {
    setForm({ ...form, [name]: value });
  };

  console.log("form", form);

  const onSubmit = () => {
    createFarmanimal(form)(farmanimalDispatch);
  };

  const formInValid =
    !form.tag?.length ||
    !form.status?.length ||
    !form.calfingcount?.length ||
    !form.birthdate?.length;

  return (
    <CreateFarmanimalUI
      onSubmit={onSubmit}
      formInvalid={formInValid}
      onChange={onChange}
      form={form}
      formIsHalfFilled={formIsHalfFilled}
      loading={loading}
    />
  );
};

export default CreateFarmanimalContainer;
