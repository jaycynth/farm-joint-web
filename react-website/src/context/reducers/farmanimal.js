import {
    FARMANIMAL_LOADING,
    FARMANIMAL_LOAD_SUCCESS,
    FARMANIMAL_LOAD_ERROR,
    ADD_FARMANIMAL_LOADING,
    ADD_FARMANIMAL_LOAD_SUCCESS,
    ADD_FARMANIMAL_LOAD_ERROR,
    SEARCH_FARM_ANIMAL,
    CLEAR_ADD_FARM_ANIMAL,
} from "../../constants/actionTypes";
const farmanimal = (state, { payload, type }) => {
    switch (type) {
        case SEARCH_FARM_ANIMAL:
            {
                const searchValue = payload?.toLowerCase();
                return {
                    ...state,
                    farmanimal: {
                        ...state.farmanimal,
                        loading: false,
                        isSearchActive: !!payload.length > 0 || false,
                        foundContacts: state.farmanimal.data?.filter((item) => {
                            try {
                                console.log(
                                    "TAGG",
                                    item.tag.toLowerCase().search(searchValue) !== -1
                                );
                                return item.tag.toLowerCase().search(searchValue) !== -1;
                            } catch (error) {
                                console.log("TAGG", error);
                                return [];
                            }
                        }),
                    },
                };
            }
        case FARMANIMAL_LOADING:
            {
                return {
                    ...state,
                    farmanimal: {
                        ...state.farmanimal,
                        loading: true,
                    },
                };
            }

        case FARMANIMAL_LOAD_SUCCESS:
            {
                return {
                    ...state,
                    farmanimal: {
                        ...state.farmanimal,
                        loading: false,
                        data: payload,
                    },
                };
            }

        case FARMANIMAL_LOAD_ERROR:
            {
                return {
                    ...state,
                    farmanimal: {
                        ...state.farmanimal,
                        loading: false,
                        error: payload,
                    },
                };
            }
        case ADD_FARMANIMAL_LOADING:
            {
                return {
                    ...state,
                    addFarmanimal: {
                        ...state.addFarmanimal,
                        error: null,
                        loading: true,
                    },
                };
            }
        case ADD_FARMANIMAL_LOAD_SUCCESS:
            {
                return {
                    ...state,
                    addFarmanimal: {
                        ...state.addFarmanimal,
                        loading: false,
                        data: payload,
                    },
                    farmanimal: {
                        ...state.farmanimal,
                        loading: false,
                        data: [payload, ...state.farmanimal.data],
                    },
                };
            }
        case ADD_FARMANIMAL_LOAD_ERROR:
            {
                return {
                    ...state,
                    addFarmanimal: {
                        ...state.addFarmanimal,
                        loading: false,
                    },
                };
            }
            case CLEAR_ADD_FARM_ANIMAL: {
                return {
                  ...state,
                  addFarmanimal: {
                    ...state.addFarmanimal,
                    error: null,
                    loading: false,
                    data: null,
                  },
                };
              }
        default:
            return state;
    }
};

export default farmanimal;